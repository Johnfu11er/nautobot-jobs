import ipaddress
from nautobot.extras.jobs import *
from nautobot.dcim.models import Device, DeviceRole, DeviceType, Site, Rack, Interface
from nautobot.extras.models import Status
from nautobot.ipam.models import Prefix, IPAddress
from nautobot.tenancy.models import Tenant


class CreateDevices(Job):
    class Meta:
        name = "Expeditionary Kit Creation"
        description = "An example job to create multiple devices"
        commit_default = False

    kit_name = ObjectVar(
        description = "Select the Kit to create",
        model = Tenant,
        display_field = 'name'
    )

    
    def run(self, data, commit):

        # Variable Creation
        ##################################################
        
        STATUS_ACTIVE = Status.objects.get(slug='active')

        tgt_tenant = data["kit_name"]

        tgt_tenant_subnets = Prefix.objects.filter(tenant__name=tgt_tenant)
        
        expeditionary_site = Site.objects.get(name="Expeditionary")

        rack_name = f"{tgt_tenant.name}-RACK"
        
        mgmt_subnet = str(tgt_tenant_subnets.get(description="MGMT").prefix)
        mgmt_cidr = mgmt_subnet.split('/')[1]
        highest_usable_mgmt_ip = ipaddress.IPv4Network(mgmt_subnet)[-2]
        second_highest_usable_mgmt_ip = ipaddress.IPv4Network(mgmt_subnet)[-3]
        
        ##################################################


        def main():
            create_rack()
            create_router()
            create_switch()


        def create_rack():
            ''' Creates a rack object '''

            # Check for existing rack with specified name
            existing_rack = Rack.objects.filter(name=rack_name)
            if existing_rack:
              self.log_failure(f"Aborted creation of rack: {existing_rack[0].name}. Rack already exists")
              return

            # Create a new rack for the designated tenant
            new_rack = Rack(
                name=rack_name,
                site=expeditionary_site,
                status=STATUS_ACTIVE,
                tenant=tgt_tenant
            )
            new_rack.validated_save()

            self.log_success(f"{rack_name} created")


        def create_router():
            ''' Creates a router object'''
            router_name =  f"{tgt_tenant.name}-B-ROUTER"
            
            # Check for existing router with specified name
            existing_router = Device.objects.filter(name=router_name)
            if existing_router:
              self.log_failure(f"Aborted creation of router: {existing_router[0].name}. Router already exists")
              return
            
            # Create new router
            new_router = Device(
                name=router_name,
                device_role=DeviceRole.objects.get(name="Router"),
                device_type=DeviceType.objects.get(model="ESR6300"),
                site=expeditionary_site,
                status=STATUS_ACTIVE,
                tenant=tgt_tenant,
                rack=Rack.objects.get(name=rack_name)
            )
            new_router.validated_save()

            # Get gig0/0 interface object
            router_gi0_0 = Interface.objects.get(
                    device__name=router_name,
                    name="GigabitEthernet0/0"
                )

            # Create primary address
            router_primary_ip = IPAddress(
                address=f"{highest_usable_mgmt_ip}/{mgmt_cidr}",
                status=STATUS_ACTIVE,
                tenant=tgt_tenant,
                assigned_object=router_gi0_0,
            )
            router_primary_ip.validated_save()

            # Set router's primary IP
            new_router.primary_ip4 = router_primary_ip
            new_router.validated_save()

            self.log_success(f"{router_name} created")


        # Create a Switch
        def create_switch():
            ''' Creates a switch object '''
            
            switch_name =  f"{tgt_tenant.name}-B-SWITCH"
            
            # Check for existing switch with specified name
            existing_switch = Device.objects.filter(name=switch_name)
            if existing_switch:
              self.log_failure(f"Aborted creation of switch: {existing_switch[0].name}. Switch already exists")
              return

            # Create new switch
            new_switch = Device(
                name=switch_name,
                device_role=DeviceRole.objects.get(name="Switch"),
                device_type=DeviceType.objects.get(model="ESS3300"),
                site=expeditionary_site,
                status=STATUS_ACTIVE,
                tenant=tgt_tenant,
                rack=Rack.objects.get(name=rack_name)
            )
            new_switch.validated_save()

            # Get gig0/0 interface object
            switch_gi0_0 = Interface.objects.get(
                    device__name=switch_name,
                    name="GigabitEthernet0/0"
                )

            # Create primary address
            switch_primary_ip = IPAddress(
                address=f"{second_highest_usable_mgmt_ip}/{mgmt_cidr}",
                status=STATUS_ACTIVE,
                tenant=tgt_tenant,
                assigned_object=switch_gi0_0,
            )
            switch_primary_ip.validated_save()

            # Set switch's primary IP
            new_switch.primary_ip4 = switch_primary_ip
            new_switch.validated_save()

            self.log_success(f"{switch_name} created")


        main()